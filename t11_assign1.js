let arr = [1, 2, 3, 4, 5, 6, 7, 8, 9];

function sign(x, y, z)
{
	let c=0;
	
	if(x < 0)
		c++;
	if(y < 0)
		c++;
	if(z < 0)
		c++;
	
	if(c%2 == 1)
		return '-';
	else
		return '+';
}

console.log("Sign of the product is " + sign(-10, -7, -4));