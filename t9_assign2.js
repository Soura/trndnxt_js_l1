let arr = [1, 2, 3, 4, 5, 6, 7, 8, 9];

function binary_search(x)
{
	for(let i=0; i<arr.length; i++)
	{
		if(arr[i] == x)
			return i;
	}
}

console.log("Element is at index " + binary_search(6));