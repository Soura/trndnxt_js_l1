function largest(n1, n2, n3, n4, n5)
{
	let arr = [n1, n2, n3, n4, n5];
	let max = arr[0];
	
	for(let i=1; i<arr.length; i++)
	{	
		if(arr[i] > max)
			max = arr[i];
	}
	
	return max;
}

console.log("Largest is " + largest(10, 27, 44, 35, 5));