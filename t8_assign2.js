let obj1 = {
	key1: 10,
	key2: 20,
	key3: 30,
};

let obj2 = {
	key1: 5,
	key2: 7,
	key3: 20,
};

let obj3 = {
	key1: 7,
	key2: 15,
	key3: 22,
};

let obj4 = {
	key1: 26,
	key2: 14,
	key3: 23,
};

let obj5 = {
	key1: 13,
	key2: 45,
	key3: 7,
};

let arr_of_objects = [obj1, obj2, obj3, obj4, obj5];

//Sorting based on key1

for(let i=0; i<arr_of_objects.length; i++)
{
	for(let j=i+1; j<arr_of_objects.length; j++)
	{
		if(arr_of_objects[j].key1 < arr_of_objects[i].key1)
		{
			let temp = arr_of_objects[i];
			arr_of_objects[i] = arr_of_objects[j];
			arr_of_objects[j] = temp;
		}
	}
}

console.log(arr_of_objects);