function fibonacci(n)
{
	console.log("0" + "\n1");
	
	let sum = 1, first = 0, second = 1;
	
	for(let i=0; i<n; i++)
	{
		sum = first + second;
		first = second;
		second = sum;
		console.log(sum + " ");
	}
}

console.log(fibonacci(100));